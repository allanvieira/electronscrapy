var fs = require('fs');

class Mining {
  constructor(indexingProcessed = new IndexingProcessed()){
    this._indexingProcessed = indexingProcessed;
    this.loadStopWords();
    this.loadAnew();
  }
  initialize(data){
    this._dataSet = data;
    this.clean();
    this.tokenize();
    this.removeStopWords();
    this.countTokens();
    this.calcBigrams();
    this.calcFeelings();
    this.calcAvgFelling();
    this.insertUpdate();
  }
  loadStopWords(){this._stopWords = JSON.parse(fs.readFileSync(__dirname+'/src/assets/data/stop_words.json', 'utf8'))}
  loadAnew(){this._anew = JSON.parse(fs.readFileSync(__dirname+'/src/assets/data/anew_br.json', 'utf8'))}
  clean(){
    this._dataSet.map( data => {
      data.summary = data.summary.replace(/-/g, ' ');
      data.summary = data.summary.replace(/[.,\/#!?$%\^&\*;:{}=\-_–`~ºª+|·“()…\[\]\"\']/g,"");

      //data.summary = data.summary.replace(/[^a-zA-Z0-9 ]/g,'')
      data.summary = data.summary.replace(/\s\s+/g, ' ');
      data.summary = data.summary.replace(/\d/g, '');
      data.summary = data.summary.replace(/\s\s\s/g, ' ');
      data.summary = data.summary.replace(/\s\s/g, ' ');
      data.summary = data.summary.replace(/\s\s/g, ' ');
      data.summary = data.summary.toLowerCase();
      if(data.summary[data.summary.length -1] == ' '){
        data.summary = data.summary.substr(0,data.summary.length -1);
      }
    })
  }
  tokenize(){
    this._dataSet.map( data => {
      data.tokens = data.summary.split(' ');
    });
  }
  removeStopWords(){
    this._dataSet.map( data => {
      data.stopWords = data.tokens.filter( token => this._stopWords.indexOf(token) > -1 );
      data.tokens = data.tokens.filter(token => data.stopWords.indexOf(token) < 0);
    });
  }
  countTokens(){
    this._dataSet.map( data => {
      data.tokensCount = {};
      data.tokens.map( token => {
        data.tokensCount[token] = (data.tokensCount[token] || 0) +1
      });
    })
  }
  calcBigrams(){
    this._dataSet.map( data => {
      data.bigrams = [];
      for(let t in data.tokens){
        if(t < data.tokens.length - 1){
          data.bigrams.push(`${data.tokens[t]} ${data.tokens[parseInt(t)+1]}`)
        }
      }
    });
  }
  // TODO : Fazer parar de procurar quando encontrar
  calcFeelings(){
    this._dataSet.map( data => {
      data.wordFellings = [];
      data.tokens.map( token => {
        this._anew.map(word => {
          if(word.palavra === token){
            data.wordFellings.push(word)
          }
        });
      });
    });
  }
  calcAvgFelling(){
    this._dataSet.map( data => {
      let sumValence = 0;
      let sumAlert = 0;
      let count = 0;
      data.wordFellings.map( word => {
        count++;
        sumValence+= parseFloat(word.valencia_media.replace(',','.'));
        sumAlert+= parseFloat(word.alerta_media.replace(',','.'));
      });
      data.avgValence = (sumValence / count) || 0;
      data.avgAlert = (sumAlert / count) || 0;
    });
  }
  insertUpdate(){
    this._indexingProcessed.insertUpdate(this._dataSet)
  }


  // acessores
  get dataSet () {return this._dataSet}
  set dataSet (v) {this._dataSet = v}
  get stopWords () {return this._stopWords}
}

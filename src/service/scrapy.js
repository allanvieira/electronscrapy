class Scrapy {
  constructor(term='dilma',date='01/01/2012',indexing = new Indexing(),mining = new Mining()){
    this._loaded = 0,
    this._maxLoad = 10,
    this._mining = mining,
    this._indexing = indexing,
    this._term = term,
    this._date = date,
    this._browser = document.getElementById('browserscrapy'),
    this._url='http://google.com.br';
    this._data = [];
    this._finish = false;
    this._browser.addEventListener('console-message', (e) => {
      if(e.message.indexOf('data$$$$') > -1){
        this._loaded++;
        var dataReturn = JSON.parse(e.message.split('$$$$')[1])
        this._data = this._data.concat(dataReturn.data);
        this._indexing.insertUpdate(dataReturn.data,function(data){
          this._mining.initialize(data);
        }.bind(this));
        if(dataReturn.next /*&& this._loaded <= this._maxLoad*/){
          this.nextScript();
        }else{
          this.nextDate();
        }
      }else{
        console.log(e.message)
      }
    });
    this._browser.addEventListener("dom-ready", () => {
      this.extractScript();
    });
    this._browser.addEventListener("update-target-url", () => {
      this.extractScript();
    });
    this.calcUrl()
  }
  calcUrl(){ this._url = `https://www.google.com.br/search?q=${this._term}&tbs=cdr:1,cd_min:${this._date},cd_max:${this._date}&tbm=#q=${this._term}&tbs=cdr:1,cd_min:${this._date},cd_max:${this._date},lr:lang_1pt&lr=lang_pt&filter=0`}
  loadUrl(){ this._browser.src = this._url }
  extractScript(){
    let script =  `
        var exec = document.querySelectorAll('#lst-ib').length > 0;
        if(exec){
          var containers = document.querySelectorAll('.srg .g');
          var jReturn = {};
          jReturn.next = document.querySelectorAll('a#pnnext').length > 0 ? true : false;
          jReturn.data = [];
          var initIdx = (document.querySelector('#nav td.cur').textContent - 1) * 10
          for(c in containers){
            if(typeof containers[c] === 'object'){
              var cont = containers[c];
              var title = cont.querySelector('h3.r a');
              var summary = cont.querySelector('.s .st');
              var item = {};
              item.date = '${this._date}';
              item.term = '${this._term}';
              item.idx = parseInt(c) + parseInt(initIdx) + 1;
              item.title = title.text;
              item.url = title.href;
              item.domain = /^(?:https?:\\/\\/)?(?:[^@\\/\\n]+@)?(?:www\\.)?([^:\\/\\n]+)/.exec(title.href)[1]
              item.summary = summary ? summary.textContent.substring(summary.textContent.indexOf('-')+2,summary.textContent.length) : '';
              jReturn.data.push(item);
            }
          }
          console.log('data$$$$'+JSON.stringify(jReturn))
        }
    `
    this._browser.executeJavaScript(script);
  }
  nextScript(){
    let script = `document.querySelector('a#pnnext').click()`
    this._browser.executeJavaScript(script);
    setTimeout(function(){
      this.extractScript()
    }.bind(this), 1500);
  }
  nextDate(){
    let dt = new Date(this._date.split('/')[2],this._date.split('/')[1],parseInt(this._date.split('/')[0])  +1)
    this._date = (dt.getDate()<10?'0'+dt.getDate():dt.getDate())+'/'+(dt.getMonth()<10?'0'+dt.getMonth():dt.getMonth())+'/'+dt.getFullYear()
    this.calcUrl()
    this.loadUrl()
  }

  // acessores
  get data () {return this._data}
  set data (v) {this._data = v}
  get date () {return this._date}
  set date (v) {this._date = v}
  get browser () {return this._browser}
  set browser (v) {this._browser = v}
  get url () {return this._url}
  set url (v) {this._url = v}

}

class IndexingProcessed {
  constructor () { this._dataset = []}
  insertUpdate(data){
      data.map( d => {
        let query = ` INSERT INTO indexing_processed (indexing_id,avg_alert, avg_valence, bigrams, stop_words, token, tokens_count,word_felligns )
                      VALUES (${d.id},
                        ${d.avgAlert},
                      ${d.avgValence},
                      '${JSON.stringify(d.bigrams).replace('[','{').replace(']','}')}',
                      '${JSON.stringify(d.stopWords).replace('[','{').replace(']','}')}',
                      '${JSON.stringify(d.tokens).replace('[','{').replace(']','}')}',
                      '${JSON.stringify(d.tokensCount)}',
                      '${JSON.stringify(d.wordFellings)}')
                      on conflict (indexing_id)
                      DO UPDATE SET avg_alert = EXCLUDED.avg_alert, avg_valence = EXCLUDED.avg_valence, bigrams = EXCLUDED.bigrams, stop_words = EXCLUDED.stop_words, token = EXCLUDED.token, tokens_count = EXCLUDED.tokens_count, word_felligns = EXCLUDED.word_felligns RETURNING *;`;
      client.query(query, (err, result) => {
        if (err) {
          console.log(err)
        }
      });
    })
  }
}

class Indexing {
  constructor () { this.dataset = []}
  all(callback){
    let query = `SELECT ID, indexed_at, term, index, title, url, summary, proccessed, domain FROM INDEXING`;
    client.query(query, (err, result) => {
      if (err) {
        console.log(err)
      }else{        
        callback(result.rows)
      }
    });
  }
  find ({date: d, term: t, index: i},callback){
    let query = `SELECT ID, indexed_at, term, index, title, url, summary, proccessed, domain FROM INDEXING WHERE INDEXED_AT = '${d}' AND TERM = '${t}' AND INDEX = ${i}`;
    console.log(query);
    client.query(query, (err, result) => {
      if (err) {
        console.log(err)
      }else{
        callback(result.rows)
      }
    });
  }
  insertUpdate(data,callback= (data)=>{console.log(data)}){
      let records = [];
      data.map( d => {
        records.push(`('${d.date}','${d.term.replace(/\'/g,'')}',${d.idx},'${d.title.replace(/\'/g,'')}','${d.url}','${d.domain}','${d.summary.replace(/\'/g,'')}')`)
      });
      let query = ` INSERT INTO indexing (INDEXED_AT, TERM, INDEX, TITLE, URL, DOMAIN, SUMMARY )
                    VALUES ${records.map(String).join(',')}
                    on conflict (INDEXED_AT,TERM, INDEX)
                    DO UPDATE SET TITLE = EXCLUDED.TITLE, URL = EXCLUDED.URL, DOMAIN = EXCLUDED.DOMAIN, SUMMARY = EXCLUDED.SUMMARY RETURNING *;`;
      client.query(query, (err, result) => {
        if (err) {
          console.log(err)
        }else{
          callback(result.rows);
        }
      });

  }
}
